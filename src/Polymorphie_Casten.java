/**
 *
 * @author Hannes
 */
public class Polymorphie_Casten {

    public static void main(String[] args) {
        TV tv0 = new TV();
        
        
        //Up Casten (Impliziet)
        
        Gerät g0 = new TV(); // TV ist ein Gerät also wird es hoch gecastet
        //oder
        Gerät g1 = tv0;
        
        g0.start(); // g0 ist vom  TV und kennt die Methode von TV
        g1.start(); // g1 ist vom  TV und kennt die Methode von TV
        
        /*
        Error: g0.SenderWechseln();
        Error: g1.SenderWechseln();
        
        g0 und g1 ist vom Typ Gerät, deshalb kennt es nicht die Methoden vom TV.
        
        dh. g0 und g1 kennen nur die Methoden die in beiden Klassen
        vorhanden sind ( in diesem Fall start). Wenn das der Fall ist 
        überschreibt die Sohnklasse(TV) die start Methode.
        */
           
        
        //Down Casten (Kann unsicher sein)
        
        Gerät g2 = new Radio();// Impliziter Up-Cast        
        /*
        Vom Typ ist das Radio ein Gerät, um nun die Methoden sichtbar zu machen
        ,die Radio hat, muss ich Explizit Down Casten.
        */
        
        Radio r1 = (Radio) g2; //Expliziter (durch die angbe "Radio") down Cast
        r1.start();
        r1.FqWechseln(); // Jetzt weiß das r1 ein Radio ist.
        
        /*
        Achtung DownCasten kann auch gefährlich werden.
        Folgendes Beispiel: Runtime Error
        */
        
        Gerät g3 = new Gerät();
        //Radio r2 = (Radio) g3; // Laut java vorerstmal okay
        //g3.start(); // aber Das Gerät ist ein Gerät Objekt  und kann nicht als
        //Radio gecastet werden.
        
        
        
    }
}






class Gerät{
    public void start(){
        System.out.println("Gerät gestartet");
    }
}

class TV extends Gerät{
    public void start(){
        System.out.println("TV gestartet");
    }
    public void SenderWechseln(){
        System.out.println("Frequenz gewechselt");
    }
}

class Radio extends Gerät{
    public void start(){
        System.out.println("Radio gestartet");
    }
    public void FqWechseln(){
        System.out.println("Frequenz gewechselt");
    }
}
